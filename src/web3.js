import Web3 from 'web3';
import farmAbi from './abis/Farm.json';
import erc20Abi from './abis/ERC20.json';
const network = 'rinkeby';
export const farmAddress = '0x50aCbf689ca9164Df76DBD524c7500AEDfD2E9da';
export const usdtAddress = '0xA8036b0eD38254B3152a3528bd176D19E76F2061';


class Web3App {
    constructor() {
        if (window.ethereum) {
            this.web3 = new Web3(window.web3.currentProvider);
            this.farm = new this.web3.eth.Contract(farmAbi.abi, farmAddress);
            this.erc20 = new this.web3.eth.Contract(erc20Abi.abi, usdtAddress);
        }
    }
}

export default new Web3App();