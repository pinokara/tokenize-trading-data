import React, { useState, useEffect } from 'react';
import { Table, Button } from 'antd';
import './App.css';
import axios from 'axios';
import web3App, { farmAddress, usdtAddress } from './web3';

const columnsBid = [
  {
    width: 300,
    title: 'Size',
    dataIndex: 'amount',
    key: 'amount',
  },
  {
    width: 300,
    title: 'Bid',
    dataIndex: 'price',
    key: 'price',
  },
];

const columnsAsk = [
  {
    width: 300,
    title: 'Ask',
    dataIndex: 'price',
    key: 'price',
  },
  {
    width: 300,
    title: 'Size',
    dataIndex: 'amount',
    key: 'amount',
  },
];



function App() {
  const [dataBids, setDataBid] = useState([]);
  const [dataAsks, setDataAsk] = useState([]);
  const [approve, setApprove] = useState(0);
  const [allowance, setAllowance] = useState(0);
  const [deposit, setDeposit] = useState(0);
  const [defaultAddress, setDefaultAddress] = useState(null);

  const handleAccountChange = (accounts) => {
    if (accounts.length === 0) {
      console.log('Please connect to MetaMask.');
    } else if (accounts[0] !== defaultAddress) {
      localStorage.setItem('address', accounts[0]);
      setDefaultAddress(accounts[0]);
    }
  }
  const connect = async () => {
    if (window.ethereum) {
      window.ethereum.request({ method: 'eth_requestAccounts' })
        .then(handleAccountChange);
    }
  }

  const approveToken = async () => {
    if (window.ethereum) {
      const bn = web3App.web3.utils.toWei(approve.toString(), 'wei');
      const data = await web3App.erc20.methods.approve(farmAddress, bn).encodeABI();
      const transactionParameters = {
        to: farmAddress, // Required except during contract publications.
        from: window.ethereum.selectedAddress, // must match user's active address.
        data, // Optional, but used for defining smart contract creation and interaction.
      };
      
      // txHash is a hex string
      // As with any RPC call, it may throw an error
      const txHash = await window.ethereum.request({
        method: 'eth_sendTransaction',
        params: [transactionParameters],
      });
    }
  }
  const depositToken = async () => {
    if (window.ethereum) {
      const bn = web3App.web3.utils.toWei(deposit.toString(), 'wei');
      const data = await web3App.farm.methods.deposit(bn).encodeABI();
      const transactionParameters = {
        to: farmAddress, // Required except during contract publications.
        from: window.ethereum.selectedAddress, // must match user's active address.
        data, // Optional, but used for defining smart contract creation and interaction.
      };
      
      // txHash is a hex string
      // As with any RPC call, it may throw an error
      const txHash = await window.ethereum.request({
        method: 'eth_sendTransaction',
        params: [transactionParameters],
      });
    }
  }

  const getAllowance = async (address) => {
    const allowance = await web3App.erc20.methods.allowance(usdtAddress, address).call();
    setAllowance(allowance);
  };

  useEffect(() => {
    if (localStorage.getItem('address') !== null) {
      setDefaultAddress(localStorage.getItem('address'))
      getAllowance(localStorage.getItem('address'));
    }
  }, []);

  const fetchData = async () => {
    const { data } = await axios.get('https://api.binance.com/api/v3/depth?symbol=ETHBTC&limit=10');
    const dataBids = data.bids.map(([price, amount], key) => ({
      key,
      price,
      amount,
    }));
    const dataAsks = data.asks.map(([price, amount], key) => ({
      key,
      price,
      amount,
    }));
    setDataBid(dataBids);
    setDataAsk(dataAsks);
  }
  useEffect(() => {
    const request = setInterval(() => {
      fetchData();
    }, 3000);
    return () => clearInterval(request);
  }, []);
  return (
    <div>
      <div className="App" style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 20,
        marginBottom: 30,
      }}>
        <Table dataSource={dataBids} columns={columnsBid} className="table-bid" bordered/>
        <Table dataSource={dataAsks} columns={columnsAsk} bordered/>
      </div>

      <div style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
      }}>
        <div style={{ marginBottom: 10 }}>
          {
            defaultAddress ? (
              <Button disabled>{defaultAddress}</Button>
            ) : (
              <Button onClick={connect}>Connect</Button>
            )
          }
        </div>
        <div style={{ marginBottom: 10 }}>
          {
            defaultAddress && (
              <><Button onClick={approveToken}>Approve</Button> <input value={approve} onChange={(e) => {
                setApprove(e.target.value);
              }}/></>
            )
          }
        </div>
        <div>
          {
            defaultAddress && (
              <><Button disaled={allowance > 0} onClick={depositToken}>Deposit To Farm</Button> <input value={deposit} onChange={(e) => {
                setDeposit(e.target.value);
              }}/></>
            )
          }
        </div>
      </div>
    </div>
  );
}

export default App;
